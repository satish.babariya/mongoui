
/**
 * Module dependencies.
 */

var express        = require('express');
var session        = require('cookie-session');
var favicon        = require('serve-favicon');
var bodyParser     = require('body-parser');
var morgan         = require('morgan');
var methodOverride = require('method-override');
var basicAuth      = require('basic-auth');
var passport       = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var http = require('http');
var path = require('path');

var ENV;
if (process.env.NODE_ENV) {
  ENV = process.env.NODE_ENV
} else {
  ENV = 'development';
}

// Config
var config = require('./config/config.json');


var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
    mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL,
    mongoURLLabel = "";

if (mongoURL == null) {
  var mongoHost, mongoPort, mongoDatabase, mongoPassword, mongoUser;
  // If using plane old env vars via service discovery
  if (process.env.DATABASE_SERVICE_NAME) {
    var mongoServiceName = process.env.DATABASE_SERVICE_NAME.toUpperCase();
    mongoHost = process.env[mongoServiceName + '_SERVICE_HOST'];
    mongoPort = process.env[mongoServiceName + '_SERVICE_PORT'];
    mongoDatabase = process.env[mongoServiceName + '_DATABASE'];
    mongoPassword = process.env[mongoServiceName + '_PASSWORD'];
    mongoUser = process.env[mongoServiceName + '_USER'];

  // If using env vars from secret from service binding  
  } else if (process.env.database_name) {
    mongoDatabase = process.env.database_name;
    mongoPassword = process.env.password;
    mongoUser = process.env.username;
    var mongoUriParts = process.env.uri && process.env.uri.split("//");
    if (mongoUriParts.length == 2) {
      mongoUriParts = mongoUriParts[1].split(":");
      if (mongoUriParts && mongoUriParts.length == 2) {
        mongoHost = mongoUriParts[0];
        mongoPort = mongoUriParts[1];
      }
    }
  }

  if (mongoHost && mongoPort && mongoDatabase) {
    mongoURLLabel = mongoURL = 'mongodb://';
    if (mongoUser && mongoPassword) {
      mongoURL += mongoUser + ':' + mongoPassword + '@';
    }
    // Provide UI label that excludes user id and pw
    mongoURLLabel += mongoHost + ':' + mongoPort + '/' + mongoDatabase;
    mongoURL += mongoHost + ':' +  mongoPort + '/' + mongoDatabase;
  }
}


// MongoDB setup
var mongo = require('./config/mongo.json');
var mongoose  = require('mongoose');
var mongoUri = process.env.MONGOHQ_URL || mongo[ENV].uri;
var mongoOpts = mongo[ENV].opts;
mongoose.connect(mongoURL);

// Auth setup
var auth = require('./config/auth.json')[ENV];
global.auth = auth;
passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(obj, done) {
  done(null, obj);
});
passport.use(new GoogleStrategy({
    clientID: auth.google.client_id,
    clientSecret: auth.google.client_secret,
    callbackURL: auth.google.callback_url
  },
  function(accessToken, refreshToken, profile, done) {
    process.nextTick(function () {
      return done(null, profile);
    });
  }
));

var routes = require('./routes');
var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(morgan('dev'));
app.use(bodyParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(methodOverride());
app.use(session({keys: config.session_keys, secureProxy: false}))
app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next) {
  if(auth.basic.enabled) {
    var credentials = basicAuth(req);
    if(!credentials || !(auth.basic.name === credentials.name &&
        auth.basic.pass === credentials.pass)) {
      res.writeHead(401, {
        'WWW-Authenticate': 'Basic realm="Welcome to Mongri"'
      });
      res.end();
    } else {
      next();
    }
  } else {
    next();
  }
});

app.use(function(req, res, next){
  res.locals.isAuthenticated = req.isAuthenticated();
  res.locals.logoTitle = config.title;
  res.locals.dropCollection = config.drop_collection;
  next();
});

// development only
if ('development' == app.get('env')) {
  mongoose.set('debug', true);
}

var collection = require('./routes/collection');
var document = require('./routes/document');

app.get('/', routes.index);
app.all('/collections*', ensureAuthenticated);
app.get('/collections', collection.collections);
app.get('/collections/:collection/drop', collection.dropCollection);
app.get('/collections/:document', document.documents);
app.get('/collections/:document/new', document.newDocument);
app.post('/collections/:document', document.postDocument);
app.get('/collections/:document/:id', document.document);
app.delete('/collections/:document/:id', document.deleteDocument);
app.post('/collections/:document/:id', document.documentUpdate);

app.get('/auth/google',
  passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.email'] }),
  function(req, res){});
app.get('/auth/google/callback',
  passport.authenticate('google', { failureRedirect: '/' }),
  function(req, res) {
    email = req.user.emails[0].value
    domain = email.split("@")[1]
    if (domain != auth.google.allow_domain){
      res.redirect('/logout');
    } else {
      res.redirect('/');
    }
  });
app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

function ensureAuthenticated(req, res, next) {
  if (auth.google.enabled){
    if (req.isAuthenticated()) {
      return next();
    }
    res.redirect('/');
  }else{
    next();
  }
}

http.createServer(app).listen(app.get('port'), '127.0.0.1', function(){
  console.log('Express server listening on port ' + app.get('port'));
});
